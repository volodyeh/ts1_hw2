package cz.cvut.fel.pjv;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Lab01Test {

    @Test
    void calculateSum() {
        Lab01 lab = new Lab01();
        assertEquals(
            7.,
            lab.calculateResult(1, 2., 5.)
        );
    }

    @Test
    void calculateSubtraction() {
        Lab01 lab = new Lab01();
        assertEquals(
            -3.,
            lab.calculateResult(2, 2., 5.)
        );
    }

    @Test
    void calculateMultiplication() {
        Lab01 lab = new Lab01();
        assertEquals(
            10.,
            lab.calculateResult(3, 2., 5.)
        );
    }

    @Test
    void calculateDivision() {
        Lab01 lab = new Lab01();
        assertEquals(
            10.,
            lab.calculateResult(4, 50., 5.)
        );
    }

    @Test
    void calculateDivisionByZero() {
        Lab01 lab = new Lab01();
        assertThrows(
            IllegalArgumentException.class,
            () -> lab.calculateResult(4, 50., 0.)
        );
    }

}