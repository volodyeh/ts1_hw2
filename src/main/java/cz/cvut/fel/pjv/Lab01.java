package cz.cvut.fel.pjv;

import java.util.Scanner;

public class Lab01 {

    public void homework() {
        Scanner scanner = new Scanner(System.in);

        int chosenOperation = takeOperation(scanner);

        double firstNumber = takeFirstNumber(scanner, chosenOperation);

        double secondNumber = takeSecondNumber(scanner, chosenOperation);

        if (chosenOperation == 4 && secondNumber == 0.0) {
            System.out.println("Pokus o deleni nulou!");
            System.exit(0);
        }

        int decimalAfterComma = takeDecimalNumberAfterCommaCount(scanner);

        printResult(
                chosenOperation, decimalAfterComma, firstNumber, secondNumber,
                calculateResult(
                    chosenOperation, firstNumber, secondNumber
                )
        );
    }

    private int takeOperation(Scanner scanner) {
        System.out.println("Vyber operaci (1-soucet, 2-rozdil, 3-soucin, 4-podil):");
        int chosenOperation = scanner.nextInt();

        if (chosenOperation < 1 || chosenOperation > 4) {
            System.out.println("Chybna volba!");
            System.exit(0);
        }

        return chosenOperation;
    }

    private double takeFirstNumber(Scanner scanner, int chosenOperation) {
        if (chosenOperation == 1) {
            System.out.println("Zadej scitanec: ");
        } else if (chosenOperation == 2) {
            System.out.println("Zadej mensenec: ");
        } else if (chosenOperation == 3) {
            System.out.println("Zadej cinitel: ");
        } else {
            System.out.println("Zadej delenec: ");
        }
        return scanner.nextDouble();
    }

    private double takeSecondNumber(Scanner scanner, int chosenOperation) {
        if (chosenOperation == 1) {
            System.out.println("Zadej scitanec: ");
        } else if (chosenOperation == 2) {
            System.out.println("Zadej mensitel: ");
        } else if (chosenOperation == 3) {
            System.out.println("Zadej cinitel: ");
        } else {
            System.out.println("Zadej delitel: ");
        }
        return scanner.nextDouble();
    }

    private int takeDecimalNumberAfterCommaCount(Scanner scanner) {
        System.out.println("Zadej pocet desetinnych mist: ");
        int decimalAfterComma = scanner.nextInt();

        if (decimalAfterComma < 0) {
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            System.exit(0);
        }
        return decimalAfterComma;
    }

    double calculateResult(int operation, double firstNumber, double secondNumber) {
        if (operation == 4 && secondNumber == 0) {
            throw new IllegalArgumentException("Trying to divide by zero");
        }

        return switch (operation) {
            case 1 -> firstNumber + secondNumber;
            case 2 -> firstNumber - secondNumber;
            case 3 -> firstNumber * secondNumber;
            case 4 -> firstNumber / secondNumber;
            default -> throw new IllegalArgumentException("Wrong operation. Expected 1, 2, 3, or 4.");
        };
    }

    private char getSignBasedOnOperation(int operation) {
        return switch (operation) {
            case 1 -> '+';
            case 2 -> '-';
            case 3 -> '*';
            case 4 -> '/';
            default -> throw new IllegalArgumentException("Wrong operation. Expected 1, 2, 3, or 4.");
        };
    }

    private void printResult(int operation, int decimalAfterComma, double firstNumber, double secondNumber, double result) {
        String outputPatternForNumber = "%." + decimalAfterComma + "f";

        char sign = getSignBasedOnOperation(operation);

        System.out.printf(
                outputPatternForNumber + " " + sign + " " + outputPatternForNumber + " = " + outputPatternForNumber + "\n",
                firstNumber, secondNumber, result
        );
    }
}
